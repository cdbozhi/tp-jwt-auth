<?php

return [
    'secret'      => env('JWT_SECRET', ''),
    //非对称密钥
    'public_key'  => env('JWT_PUBLIC_KEY', ''),
    'private_key' => env('JWT_PRIVATE_KEY', ''),
    'password'    => env('JWT_PASSWORD', ''),
    //JWT生存时间
    'ttl'         => env('JWT_TTL', 60),
    //Refresh生存时间
    'refresh_ttl' => env('JWT_REFRESH_TTL', 20160),
    //JWT哈希算法
    'algo'        => env('JWT_ALGO', 'HS256'),
    //token获取方式，数组值靠前值优先
    'token_mode'    => ['header', 'cookie', 'param'],
    //黑名单后有效期
    'blacklist_grace_period' => env('BLACKLIST_GRACE_PERIOD', 10),
];
