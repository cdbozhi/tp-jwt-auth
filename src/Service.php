<?php


namespace cdbozhi\jwt;

use cdbozhi\jwt\command\SecretCommand;
use cdbozhi\jwt\middleware\InjectJwt;
use cdbozhi\jwt\provider\JWT as JWTProvider;

class Service extends \think\Service
{
    public function boot()
    {
        $this->commands(SecretCommand::class);

        (new JWTProvider())->register();
    }
}
