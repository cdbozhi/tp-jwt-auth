<?php

namespace cdbozhi\jwt\claim;

class Audience extends Claim
{
    protected $name = 'aud';
}
