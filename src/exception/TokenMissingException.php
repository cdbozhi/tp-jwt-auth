<?php

namespace cdbozhi\jwt\exception;

class TokenMissingException extends JWTException
{
    protected $message = 'token missing';
}
